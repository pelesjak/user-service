# user-service


This project implements the user management microservice that is part of a semestral project in the SWA course.

To run the project in docker use the following command

    docker compose up


To run tests, install dependencies from `requirements.txt` and run

    export APP_ENV=TESTING; pytest app/tests/test_main.py -v

## Author
Jakub Peleška (pelesjak@fel.cvut.cz)
