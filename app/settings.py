import os

from pydantic import BaseSettings, Field


class Settings(BaseSettings):
    db_url: str = Field(..., env="DATABASE_URL")
    eureka_URL: str = Field(..., env="EUREKA_DISCOVERY_SERVER_URI")
    ip: str = Field(..., env="HOST_IP")
    port: int = Field(..., env="HOST_PORT")


if os.getenv("APP_ENV") == "TESTING":
    settings = Settings(db_url="sqlite://", eureka_URL="", ip="", port=0)
else:
    settings = Settings()
