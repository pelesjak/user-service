from typing import List

import asyncpg
from fastapi import APIRouter, HTTPException

from app.models import UserModel, UserCreateModel, UserUpdateModel, AuthData
from app.db import DBManager
from app.logger import logger

commands_router = APIRouter()
user_router = APIRouter()
users_router = APIRouter()


# COMMANDS
@commands_router.post("/authenticate", response_model=UserModel)
async def authenticate(payload: AuthData):
    logger.info("Received request for user authentication")

    user = await DBManager.authenticate_user(payload.email, payload.password)

    if not user:
        logger.info("User authentication failed!")
        raise HTTPException(
            status_code=400, detail="Incorrect username(email) or password"
        )

    logger.info(f"User with email {user.email} authenticated!")

    return user


@commands_router.get("/health", response_model=str)
def health():
    return "ok"


# USERS
@users_router.get("/", response_model=List[UserModel])
async def get_all_users():
    return await DBManager.get_all_users()


@users_router.get("/users/", response_model=List[UserModel])
async def get_users():
    return await DBManager.get_users()


@users_router.get("/admins/", response_model=List[UserModel])
async def get_admins():
    return await DBManager.get_admins()


# USER
@user_router.post("/", response_model=UserModel, status_code=201)
async def create_user(payload: UserCreateModel):
    try:
        user_id = await DBManager.add_user(payload)
    except asyncpg.exceptions.UniqueViolationError:
        raise HTTPException(status_code=422, detail="Email is already used")
    return await DBManager.get_user(user_id)


@user_router.get("/{user_id}", response_model=UserModel)
async def get_user(user_id: str):
    user = await DBManager.get_user(user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user


@user_router.put("/{user_id}", response_model=UserModel, status_code=201)
async def update_user(user_id: str, payload: UserUpdateModel):
    user = await DBManager.get_user(user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    try:
        await DBManager.update_user(user_id, payload)
    except asyncpg.exceptions.UniqueViolationError:
        raise HTTPException(status_code=422, detail="Email is already used")

    return await DBManager.get_user(user_id)


@user_router.delete("/{user_id}", response_model=None, status_code=204)
async def delete_user(user_id: str):
    user = await DBManager.get_user(user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")

    return await DBManager.delete_user(user_id)
