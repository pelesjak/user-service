# app
fastapi==0.95.1
pydantic==1.10.9
uvicorn==0.22.0
passlib==1.7.4

# db
asyncpg==0.27.0
databases[postgresql]==0.7.0
SQLAlchemy==1.4.48
psycopg2-binary==2.9.6

# eureka client
py_eureka_client==0.11.7
nest_asyncio==1.5.6

# dev
httpx==0.24.1
pytest==7.3.2
pytest-asyncio==0.21.0
