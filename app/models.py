from enum import Enum
from datetime import datetime, date
from pydantic import BaseModel
from typing import Optional


class Role(Enum):
    USER = 1
    ADMIN = 2


class AuthData(BaseModel):
    email: str
    password: str


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    sub: str
    scopes: list[str] = []


class UserCreateModel(BaseModel):
    password: str
    role: int
    name: str
    email: str
    birth_date: Optional[date]
    phone: Optional[str]


class UserUpdateModel(BaseModel):
    name: Optional[str]
    email: Optional[str]
    birth_date: Optional[date]
    phone: Optional[str]


class UserModel(BaseModel):
    uid: str
    registered_at: datetime
    updated_at: datetime

    active: bool
    role: Role
    name: str
    email: str
    birth_date: Optional[date]
    phone: Optional[str]


class DBUserModel(UserModel):
    password_hash: str
