import sqlalchemy as sa
from databases import Database
import datetime

from app.settings import settings

metadata = sa.MetaData()

engine = sa.create_engine(settings.db_url, pool_pre_ping=True)

current_time = lambda: sa.cast(datetime.datetime.utcnow(), sa.DateTime)

user_table = sa.Table(
    "user",
    metadata,
    sa.Column(
        "uid",
        sa.String,
        primary_key=True,
        default=sa.text("gen_random_uuid()"),
        unique=True,
    ),
    sa.Column("password_hash", sa.String, nullable=False),
    sa.Column("active", sa.Boolean, nullable=False, default=sa.cast(True, sa.Boolean)),
    sa.Column("role", sa.SmallInteger, nullable=False, default=sa.cast(1, sa.Integer)),
    sa.Column(
        "registered_at", sa.DateTime, nullable=False, server_default=sa.func.now()
    ),
    sa.Column(
        "updated_at",
        sa.DateTime,
        nullable=False,
        server_default=sa.func.now(),
        onupdate=sa.func.now(),
    ),
    sa.Column("name", sa.String, nullable=False),
    sa.Column("email", sa.String, unique=True, nullable=False),
    sa.Column("birth_date", sa.Date, nullable=True),
    sa.Column("phone", sa.String(32), nullable=True),
)

db = Database(settings.db_url)
