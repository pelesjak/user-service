from typing import List
import time

from app.models import UserModel, DBUserModel, UserCreateModel, UserUpdateModel
from .db import user_table, db, metadata, engine

from app.auth import get_password_hash, verify_password


async def startup(attempt=0):
    if attempt > 10:
        raise TimeoutError()
    try:
        metadata.create_all(engine)
        await db.connect()
    except:
        time.sleep(2)
        await startup(attempt + 1)


async def shutdown():
    await db.disconnect()


# users
async def get_all_users() -> List[UserModel]:
    query = user_table.select()
    return await db.fetch_all(query=query)


async def get_users() -> List[UserModel]:
    query = user_table.select(user_table.c.role == 1)
    return await db.fetch_all(query=query)


async def get_admins() -> List[UserModel]:
    query = user_table.select(user_table.c.role == 2)
    return await db.fetch_all(query=query)


# user
async def add_user(payload: UserCreateModel) -> int:
    user_data = payload.dict()
    plain_password = user_data.pop("password")
    user_data["password_hash"] = get_password_hash(plain_password)
    query = user_table.insert(user_data).values(**user_data)
    return await db.execute(query=query)


async def authenticate_user(email: str, password: str) -> DBUserModel:
    user = await get_db_user_by_email(email)
    if not user:
        return False
    if not verify_password(password, user.password_hash):
        return False
    return user


async def get_user(id) -> UserModel:
    query = user_table.select().where(user_table.c.uid == id)
    return await db.fetch_one(query=query)


async def get_db_user(id) -> UserModel:
    query = user_table.select(user_table.c.uid == id)
    return await db.fetch_one(query=query)


async def get_user_by_email(email) -> UserModel:
    query = user_table.select().where(user_table.c.email == email)
    return await db.fetch_one(query=query)


async def get_db_user_by_email(email) -> UserModel:
    query = user_table.select(user_table.c.email == email)
    return await db.fetch_one(query=query)


async def update_user(id, update: UserUpdateModel):
    update_values = {k: v for k, v in update.dict().items() if v is not None}
    query = user_table.update().where(user_table.c.uid == id).values(**update_values)
    return await db.execute(query=query)


async def delete_user(id):
    query = user_table.delete().where(user_table.c.uid == id)
    await db.execute(query=query)
    return
