from fastapi.testclient import TestClient
from fastapi.encoders import jsonable_encoder

import asyncpg

from app.db import DBManager
from app.models import UserCreateModel, UserUpdateModel


def test_get_user_success(test_app: TestClient, monkeypatch):
    async def mock_get_user(user_id):
        return {
            "uid": "51025805-1d13-47b1-90b6-5fa017d12996",
            "registered_at": "2023-09-05T20:51:55.388507",
            "updated_at": "2023-09-05T20:51:55.388507",
            "active": True,
            "role": 1,
            "name": "John Doe",
            "email": "johndoe@test.com",
            "birth_date": None,
            "phone": None,
        }

    monkeypatch.setattr(DBManager, "get_user", mock_get_user)

    response = test_app.get("/api/v1/user/51025805-1d13-47b1-90b6-5fa017d12996")
    assert response.status_code == 200
    assert response.json()["name"] == "John Doe"
    assert response.json()["email"] == "johndoe@test.com"
    assert response.json()["role"] == 1
    assert response.json()["active"] == True
    assert response.json()["uid"] == "51025805-1d13-47b1-90b6-5fa017d12996"


def test_get_user_fail(test_app: TestClient, monkeypatch):
    async def mock_get_user(user_id):
        return None

    monkeypatch.setattr(DBManager, "get_user", mock_get_user)

    response = test_app.get("/api/v1/user/51025805-1d13-47b1-90b6-5fa017d12996")
    assert response.status_code == 404


def test_get_users_success(test_app: TestClient, monkeypatch):
    async def mock_get_all_users():
        return [
            {
                "uid": "51025805-1d13-47b1-90b6-5fa017d12996",
                "registered_at": "2023-09-05T20:51:55.388507",
                "updated_at": "2023-09-05T20:51:55.388507",
                "active": True,
                "role": 1,
                "name": "John Doe",
                "email": "johndoe@test.com",
                "birth_date": None,
                "phone": None,
            }
        ]

    monkeypatch.setattr(DBManager, "get_all_users", mock_get_all_users)

    response = test_app.get("/api/v1/users")
    assert response.status_code == 200
    assert len(response.json()) == 1


def test_add_user(test_app: TestClient, monkeypatch):
    async def mock_add_user(user_data):
        return "51025805-1d13-47b1-90b6-5fa017d12996"

    async def mock_get_user(user_id):
        return {
            "uid": "51025805-1d13-47b1-90b6-5fa017d12996",
            "registered_at": "2023-09-05T20:51:55.388507",
            "updated_at": "2023-09-05T20:51:55.388507",
            "active": True,
            "role": 2,
            "name": "John Doe",
            "email": "johndoe@test.com",
            "birth_date": None,
            "phone": None,
        }

    monkeypatch.setattr(DBManager, "add_user", mock_add_user)
    monkeypatch.setattr(DBManager, "get_user", mock_get_user)

    response = test_app.post(
        "/api/v1/user",
        json=jsonable_encoder(
            UserCreateModel(
                role=2,
                name="John Doe",
                email="johndoe@test.com",
                password="testpassword",
            )
        ),
    )
    assert response.status_code == 201
    assert response.json()["name"] == "John Doe"
    assert response.json()["email"] == "johndoe@test.com"
    assert response.json()["role"] == 2
    assert response.json()["active"] == True


def test_add_user_duplicate_email(test_app: TestClient, monkeypatch):
    async def mock_add_user(user_data):
        raise asyncpg.exceptions.UniqueViolationError

    monkeypatch.setattr(DBManager, "add_user", mock_add_user)

    response = test_app.post(
        "/api/v1/user",
        json=jsonable_encoder(
            UserCreateModel(
                role=1,
                name="John Doe",
                email="johndoe@test.com",
                password="testpassword",
            )
        ),
    )
    assert response.status_code == 422


def test_update_user(test_app: TestClient, monkeypatch):
    async def mock_update_user(user_id, user_data):
        return

    async def mock_get_user(user_id):
        return {
            "uid": "51025805-1d13-47b1-90b6-5fa017d12996",
            "registered_at": "2023-09-05T20:51:55.388507",
            "updated_at": "2023-09-05T20:51:55.388507",
            "active": True,
            "role": 2,
            "name": "John Moe",
            "email": "johndoe@test.com",
            "birth_date": None,
            "phone": None,
        }

    monkeypatch.setattr(DBManager, "update_user", mock_update_user)
    monkeypatch.setattr(DBManager, "get_user", mock_get_user)

    response = test_app.put(
        "/api/v1/user/51025805-1d13-47b1-90b6-5fa017d12996",
        json=jsonable_encoder(
            UserUpdateModel(
                name="John Moe",
            )
        ),
    )
    assert response.status_code == 201
    assert response.json()["name"] == "John Moe"


def test_update_user_invalid_id(test_app: TestClient, monkeypatch):
    async def mock_get_user(user_id):
        return

    monkeypatch.setattr(DBManager, "get_user", mock_get_user)

    response = test_app.put(
        "/api/v1/user/51025805-1d13-47b1-90b6-5fa017d12996",
        json=jsonable_encoder(
            UserUpdateModel(
                name="John Moe",
            )
        ),
    )
    assert response.status_code == 404


def test_update_user_duplicate_email(test_app: TestClient, monkeypatch):
    async def mock_update_user(user_id, user_data):
        raise asyncpg.exceptions.UniqueViolationError

    async def mock_get_user(user_id):
        return {
            "uid": "51025805-1d13-47b1-90b6-5fa017d12996",
            "registered_at": "2023-09-05T20:51:55.388507",
            "updated_at": "2023-09-05T20:51:55.388507",
            "active": True,
            "role": 2,
            "name": "John Moe",
            "email": "johndoe@test.com",
            "birth_date": None,
            "phone": None,
        }

    monkeypatch.setattr(DBManager, "update_user", mock_update_user)
    monkeypatch.setattr(DBManager, "get_user", mock_get_user)

    response = test_app.put(
        "/api/v1/user/51025805-1d13-47b1-90b6-5fa017d12996",
        json=jsonable_encoder(
            UserUpdateModel(
                email="already_in_db@test.com",
            )
        ),
    )
    assert response.status_code == 422


def test_delete_user(test_app: TestClient, monkeypatch):
    async def delete_user(user_id):
        return

    async def mock_get_user(user_id):
        return {
            "uid": "51025805-1d13-47b1-90b6-5fa017d12996",
            "registered_at": "2023-09-05T20:51:55.388507",
            "updated_at": "2023-09-05T20:51:55.388507",
            "active": True,
            "role": 1,
            "name": "John Moe",
            "email": "johndoe@test.com",
            "birth_date": None,
            "phone": None,
        }

    monkeypatch.setattr(DBManager, "delete_user", delete_user)
    monkeypatch.setattr(DBManager, "get_user", mock_get_user)

    response = test_app.delete(
        "/api/v1/user/51025805-1d13-47b1-90b6-5fa017d12996",
    )
    assert response.status_code == 204


def test_delete_user_invalid_id(test_app: TestClient, monkeypatch):
    async def mock_get_user(user_id):
        return

    monkeypatch.setattr(DBManager, "get_user", mock_get_user)

    response = test_app.delete(
        "/api/v1/user/51025805-1d13-47b1-90b6-5fa017d12996",
    )
    assert response.status_code == 404
