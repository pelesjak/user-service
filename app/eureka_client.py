from app.settings import settings

import py_eureka_client.eureka_client as eureka_client
import nest_asyncio

nest_asyncio.apply()  # needed for eureka_client to connect to the server


def register_service():
    EUREKA_DISCOVERY_SERVER_URI = settings.eureka_URL
    HOST_IP = settings.ip
    HOST_PORT = settings.port
    SERVICE_URI = f"http://{HOST_IP}:{HOST_PORT}"

    # register this service to eureka server and start to send heartbeat every 30 seconds
    eureka_client.init(
        eureka_server=EUREKA_DISCOVERY_SERVER_URI,
        app_name="user-service",
        instance_ip=HOST_IP,
        instance_port=HOST_PORT,
        home_page_url=f"{SERVICE_URI}/api/v1",
        status_page_url=f"{SERVICE_URI}/api/v1/docs",
        health_check_url=f"{SERVICE_URI}/api/v1/health",
    )


def stop_and_unregister_service():
    return eureka_client.stop()
