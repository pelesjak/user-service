from fastapi import FastAPI

from app.api import commands_router, user_router, users_router
from app.db import DBManager

import app.eureka_client as eureka_client

app = FastAPI(openapi_url="/api/v1/openapi.json", docs_url="/api/v1/docs")


@app.on_event("startup")
async def startup():
    await DBManager.startup()
    eureka_client.register_service()


@app.on_event("shutdown")
async def shutdown():
    await DBManager.shutdown()
    eureka_client.stop_and_unregister_service()


app.include_router(commands_router, prefix="/api/v1", tags=["commands"])
app.include_router(user_router, prefix="/api/v1/user", tags=["user"])
app.include_router(users_router, prefix="/api/v1/users", tags=["users"])
