import logging

logger = logging.getLogger(__name__)

handler = logging.StreamHandler()
handler.setFormatter(logging.Formatter("%(levelname)s:     %(asctime)s - %(message)s"))
logger.addHandler(handler)
logger.setLevel(level=logging.INFO)
